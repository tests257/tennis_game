part of '../ui_kit.dart';

class AppTapUnfocus extends StatelessWidget {
  final Widget child;
  final Function()? onFocusLost;

  const AppTapUnfocus({required this.child, this.onFocusLost});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        if (FocusScope.of(context).focusedChild != null) {
          FocusScope.of(context).focusedChild!.unfocus();
          onFocusLost?.call();
        }
      },
      child: child,
    );
  }
}
