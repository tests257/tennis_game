part of '../ui_kit.dart';

extension ThemeMethods on BuildContext {
  ThemeData get theme => Theme.of(this);
}
