// ignore_for_file: avoid_classes_with_only_static_members
part of '../ui_kit.dart';

class AppValidators {
  static FormFieldValidator<String> fullNameValidator() =>
      FormBuilderValidators.compose([
        FormBuilderValidators.required(errorText: "Shouldn't be empty"),
        FormBuilderValidators.match(
          r"^[A-Z]{3,} [A-Z]{3,}$",
          errorText: 'Wrong pattern',
        ),
      ]);
}
