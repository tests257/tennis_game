part of '../ui_kit.dart';

class AppSvgImage extends StatelessWidget {
  final String assetPath;
  final double? width;
  final double? height;
  final BoxFit fit;
  final Color? color;

  const AppSvgImage(
    this.assetPath, {
    super.key,
    this.color,
    this.width,
    this.height,
    this.fit = BoxFit.contain,
  });

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      child: SvgPicture.asset(
        assetPath,
        color: color,
        width: width,
        height: height,
      ),
    );
  }
}
