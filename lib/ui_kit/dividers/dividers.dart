part of '../ui_kit.dart';

class AppVerticalDivider extends StatelessWidget {
  const AppVerticalDivider({super.key});

  @override
  Widget build(BuildContext context) {
    return const VerticalDivider(
      thickness: 3.0,
      width: 3.0,
      color: Colors.white30,
    );
  }
}
