part of 'models.dart';

class Player extends Equatable {
  final String? fullName;

  const Player([this.fullName]);

  Player copyWith({String? name}) => Player(name ?? fullName);

  bool get isValid => AppValidators.fullNameValidator()(fullName) == null;

  @override
  List<Object?> get props => [fullName];
}
