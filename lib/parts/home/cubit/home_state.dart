part of '../home_part.dart';

class HomeState extends Equatable {
  final Player firstPlayer;
  final Player secondPlayer;
  final bool isPlaying;

  const HomeState({
    required this.firstPlayer,
    required this.secondPlayer,
    required this.isPlaying,
  });

  const HomeState.initial()
      : firstPlayer = const Player(),
        secondPlayer = const Player(),
        isPlaying = false;

  HomeState copyWith({
    Player? firstPlayer,
    Player? secondPlayer,
    bool? isPlaying,
  }) =>
      HomeState(
        firstPlayer: firstPlayer ?? this.firstPlayer,
        secondPlayer: secondPlayer ?? this.secondPlayer,
        isPlaying: isPlaying ?? this.isPlaying,
      );

  @override
  List<Object?> get props => [firstPlayer, secondPlayer, isPlaying];
}
