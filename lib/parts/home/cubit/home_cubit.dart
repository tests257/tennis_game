part of '../home_part.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(const HomeState.initial());

  void gameStarted({
    required Player firstPlayer,
    required Player secondPlayer,
  }) =>
      emit(
        HomeState(
          firstPlayer: firstPlayer,
          secondPlayer: secondPlayer,
          isPlaying: true,
        ),
      );

  void gameStopped() => emit(state.copyWith(isPlaying: false));
}

extension HomeCubitBuildContextX on BuildContext {
  HomeCubit get homeCubit => read<HomeCubit>();
}
