part of '../home_part.dart';

class PlayerTextField extends StatelessWidget {
  final String fieldName;
  final String? initialValue;

  const PlayerTextField({
    super.key,
    required this.fieldName,
    this.initialValue,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FormBuilderTextField(
        name: fieldName,
        initialValue: initialValue,
        textAlign: TextAlign.center,
        decoration: const InputDecoration(
          hintText: 'Surname Name',
          contentPadding: EdgeInsets.only(left: 8.0, right: 8.0),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
          ),
          errorStyle: TextStyle(
            height: 0.0,
            fontSize: 0.01,
            color: Colors.transparent,
          ),
        ),
        validator: AppValidators.fullNameValidator(),
      ),
    );
  }
}
