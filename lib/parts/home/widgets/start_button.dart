part of '../home_part.dart';

class StartButton extends StatelessWidget {
  final Size size;
  final VoidCallback? onPressed;

  const StartButton({
    super.key,
    this.size = const Size(128.0, 128.0),
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: size.width,
      height: size.height,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          shape: const CircleBorder(),
          backgroundColor: context.theme.primaryColor,
        ),
        onPressed: onPressed,
        child: const Text('Start'),
      ),
    );
  }
}
