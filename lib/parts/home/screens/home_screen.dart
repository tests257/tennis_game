part of '../home_part.dart';

class HomeScreen extends StatefulWidget {
  final Player firstPlayer;
  final Player secondPlayer;

  const HomeScreen({
    super.key,
    required this.firstPlayer,
    required this.secondPlayer,
  });

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  static const firstPlayerField = 'first';
  static const secondPlayerField = 'second';

  final formKey = GlobalKey<FormBuilderState>();
  late bool isStartButtonAvailable =
      widget.firstPlayer.isValid && widget.secondPlayer.isValid;

  @override
  Widget build(BuildContext context) {
    return AppTapUnfocus(
      child: FormBuilder(
        key: formKey,
        onChanged: onFormChanged,
        child: Column(
          children: [
            Row(
              children: [
                PlayerTextField(
                  fieldName: firstPlayerField,
                  initialValue: widget.firstPlayer.fullName,
                ),
                const SizedBox(
                  height: 48.0,
                  child: AppVerticalDivider(),
                ),
                PlayerTextField(
                  fieldName: secondPlayerField,
                  initialValue: widget.secondPlayer.fullName,
                ),
              ],
            ),
            Expanded(
              child: StartButton(
                onPressed:
                    isStartButtonAvailable ? () => saveData(context) : null,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void onFormChanged() {
    final formBuilderState = formKey.currentState;

    bool areAllFieldsFilled = true;
    bool isFormValid = true;

    for (final field in formBuilderState!.fields.values) {
      if (areAllFieldsFilled) {
        areAllFieldsFilled = field.value != null;
      }
      if (!areAllFieldsFilled || isFormValid && !field.validate()) {
        isFormValid = false;
      }
    }

    if (isFormValid != isStartButtonAvailable) {
      setState(() {
        isStartButtonAvailable = isFormValid;
      });
    }
  }

  void saveData(BuildContext context) {
    final formBuilderState = formKey.currentState;
    if (formBuilderState?.saveAndValidate() ?? false) {
      context.homeCubit.gameStarted(
        firstPlayer:
            Player(formBuilderState!.value[firstPlayerField] as String),
        secondPlayer:
            Player(formBuilderState.value[secondPlayerField] as String),
      );
    }
  }
}
