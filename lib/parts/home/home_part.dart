library home;

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:tennis_game/code_kit/models/models.dart';
import 'package:tennis_game/gen/assets.gen.dart';
import 'package:tennis_game/parts/game/game_part.dart';
import 'package:tennis_game/ui_kit/ui_kit.dart';

part 'cubit/home_cubit.dart';
part 'cubit/home_state.dart';

part 'screens/home_screen.dart';

part 'widgets/player_text_field.dart';
part 'widgets/start_button.dart';

class HomePart extends StatelessWidget {
  const HomePart({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => HomeCubit(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          constraints: const BoxConstraints.expand(),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Assets.images.court.path),
              fit: BoxFit.cover,
            ),
          ),
          child: SafeArea(
            child: BlocBuilder<HomeCubit, HomeState>(
              builder: (context, state) {
                if (state.isPlaying) {
                  return const GamePart();
                }

                return HomeScreen(
                  firstPlayer: state.firstPlayer,
                  secondPlayer: state.secondPlayer,
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
