part of '../game_part.dart';

class Playground extends StatelessWidget {
  final Player player;
  final PlayerSide side;

  const Playground({
    super.key,
    required this.player,
    required this.side,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          Flexible(
            child: Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 14.0, horizontal: 8.0),
                child: Text(
                  player.fullName ?? 'Unknown',
                  textAlign: TextAlign.center,
                  style: context.theme.textTheme.subtitle1,
                ),
              ),
            ),
          ),
          Flexible(
            child: BlocBuilder<GameCubit, GameState>(
              builder: (context, state) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '${side == PlayerSide.left ? state.firstTotalScore : state.secondTotalScore}',
                      style: context.theme.textTheme.headline1
                          ?.copyWith(color: Colors.white),
                    ),
                    Text(
                      side == PlayerSide.left
                          ? _currentScore(
                              state.firstCurrentGameScore,
                              state.secondCurrentGameScore,
                            )
                          : _currentScore(
                              state.secondCurrentGameScore,
                              state.firstCurrentGameScore,
                            ),
                      style: context.theme.textTheme.headline4
                          ?.copyWith(color: Colors.white),
                    ),
                  ],
                );
              },
            ),
          ),
          Flexible(
            child: RotatedBox(
              quarterTurns: side == PlayerSide.left ? 3 : 1,
              child: IconButton(
                icon: AppSvgImage(
                  Assets.icons.tennisball.path,
                  height: 64.0,
                  width: 64.0,
                ),
                onPressed: () {
                  context.gameCubit.ballClicked(side);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  String _currentScore(int current, int opponentCurrent) {
    if (current > 3 && current > opponentCurrent) {
      return 'A';
    }

    if (current < 3) {
      return '${current * 15}';
    }

    return '40';
  }
}
