// ignore_for_file: unused_element

part of '../game_part.dart';

class StartGameCounter extends StatefulWidget {
  final VoidCallback onStart;

  const StartGameCounter({super.key, required this.onStart});

  @override
  State<StartGameCounter> createState() => _StartGameCounterState();
}

class _StartGameCounterState extends State<StartGameCounter> {
  int count = 3;
  Timer? timer;

  @override
  void initState() {
    timer = Timer.periodic(const Duration(milliseconds: 300), (t) {
      setState(() {
        count--;
      });

      if (count < 0) {
        t.cancel();
        widget.onStart.call();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      count < 1 ? 'GO' : '$count',
      style: context.theme.textTheme.headline1?.copyWith(color: Colors.white),
    );
  }
}
