part of '../game_part.dart';

class InterimGameScore {
  final int total;
  final int current;
  final int opponentTotal;
  final int opponentCurrent;
  final bool isWinner;
  final bool isRound;

  const InterimGameScore({
    required this.total,
    required this.current,
    required this.opponentTotal,
    required this.opponentCurrent,
    this.isWinner = false,
    this.isRound = false,
  });

  InterimGameScore copyWith({
    int? total,
    int? current,
    int? opponentTotal,
    int? opponentCurrent,
    bool? isWinner,
    bool? isRound,
  }) =>
      InterimGameScore(
        total: total ?? this.total,
        current: current ?? this.current,
        opponentTotal: opponentTotal ?? this.opponentTotal,
        opponentCurrent: opponentCurrent ?? this.opponentCurrent,
        isWinner: isWinner ?? this.isWinner,
        isRound: isRound ?? this.isRound,
      );
}
