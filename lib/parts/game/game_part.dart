library game;

import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tennis_game/code_kit/models/models.dart';
import 'package:tennis_game/gen/assets.gen.dart';
import 'package:tennis_game/parts/home/home_part.dart';
import 'package:tennis_game/ui_kit/ui_kit.dart';

part 'cubit/game_cubit.dart';
part 'cubit/game_state.dart';

part 'models/interim_game_score.dart';
part 'models/player_side.dart';

part 'screens/game_screen.dart';

part 'widgets/playground.dart';
part 'widgets/start_game_counter.dart';

class GamePart extends StatelessWidget {
  const GamePart({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => GameCubit(),
      child: BlocConsumer<GameCubit, GameState>(
        listener: (context, state) {
          if (state.winner != null) {
            _onGameOver(
              context,
              state.winner == PlayerSide.left
                  ? context.homeCubit.state.firstPlayer
                  : context.homeCubit.state.secondPlayer,
            );
          }
        },
        buildWhen: (previous, current) =>
            previous.showCountDown != current.showCountDown,
        builder: (context, state) {
          return Stack(
            children: [
              Opacity(
                opacity: 1,
                child: IgnorePointer(
                  ignoring: state.showCountDown,
                  child: const GameScreen(),
                ),
              ),
              if (state.showCountDown)
                Container(
                  alignment: Alignment.topCenter,
                  padding: const EdgeInsets.only(top: 80.0),
                  child: StartGameCounter(
                    onStart: context.gameCubit.countDownHid,
                  ),
                ),
            ],
          );
        },
      ),
    );
  }

  Future<void> _onGameOver(BuildContext context, Player winner) async {
    final result = await showDialog<bool?>(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        title: Text(
          'Winner is ${winner.fullName}',
          textAlign: TextAlign.center,
        ),
        actionsAlignment: MainAxisAlignment.spaceAround,
        actions: [
          TextButton(
            child: const Text('Play again'),
            onPressed: () => Navigator.pop(context, true),
          ),
          TextButton(
            child: const Text('Go to menu'),
            onPressed: () => Navigator.pop(context, false),
          ),
        ],
      ),
    );

    if (result != null) {
      result
          ? context.gameCubit.gameRestarted()
          : context.homeCubit.gameStopped();
    }
  }
}
