part of '../game_part.dart';

class GameCubit extends Cubit<GameState> {
  GameCubit() : super(const GameState.initial());

  void gameRestarted() => emit(const GameState.initial());

  void countDownShowed() => emit(state.copyWith(showCountDown: true));

  void countDownHid() => emit(state.copyWith(showCountDown: false));

  void ballClicked(PlayerSide side) {
    switch (side) {
      case PlayerSide.left:
        final score = _calcScore(
          InterimGameScore(
            total: state.firstTotalScore,
            current: state.firstCurrentGameScore,
            opponentTotal: state.secondTotalScore,
            opponentCurrent: state.secondCurrentGameScore,
          ),
        );

        emit(
          state.copyWith(
            firstTotalScore: score.total,
            firstCurrentGameScore: score.current,
            secondTotalScore: score.opponentTotal,
            secondCurrentGameScore: score.opponentCurrent,
            winner: score.isWinner ? PlayerSide.left : null,
          ),
        );

        break;
      case PlayerSide.right:
        final score = _calcScore(
          InterimGameScore(
            total: state.secondTotalScore,
            current: state.secondCurrentGameScore,
            opponentTotal: state.firstTotalScore,
            opponentCurrent: state.firstCurrentGameScore,
          ),
        );

        emit(
          state.copyWith(
            firstTotalScore: score.opponentTotal,
            firstCurrentGameScore: score.opponentCurrent,
            secondTotalScore: score.total,
            secondCurrentGameScore: score.current,
            winner: score.isWinner ? PlayerSide.right : null,
          ),
        );

        break;
    }
  }

  InterimGameScore _calcScore(InterimGameScore score) {
    final newCurrentScore = score.current + 1;
    if (newCurrentScore > 3 && (newCurrentScore - score.opponentCurrent) > 1) {
      final newToltalScore = score.total + 1;

      if (newToltalScore > 5 && (newToltalScore - score.opponentTotal) > 1) {
        return score.copyWith(
          total: newToltalScore,
          current: 0,
          opponentCurrent: 0,
          isWinner: true,
        );
      } else {
        countDownShowed();
        return score.copyWith(
          total: newToltalScore,
          current: 0,
          opponentCurrent: 0,
          isRound: true,
        );
      }
    } else {
      return score.copyWith(
        current: newCurrentScore,
      );
    }
  }
}

extension GameCubitBuildContextX on BuildContext {
  GameCubit get gameCubit => read<GameCubit>();
}
