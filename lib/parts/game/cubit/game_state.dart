part of '../game_part.dart';

class GameState extends Equatable {
  final bool showCountDown;
  final int firstTotalScore;
  final int secondTotalScore;
  final int firstCurrentGameScore;
  final int secondCurrentGameScore;
  final PlayerSide? winner;

  const GameState({
    required this.showCountDown,
    required this.firstTotalScore,
    required this.secondTotalScore,
    required this.firstCurrentGameScore,
    required this.secondCurrentGameScore,
    this.winner,
  });

  const GameState.initial()
      : showCountDown = true,
        firstTotalScore = 0,
        secondTotalScore = 0,
        firstCurrentGameScore = 0,
        secondCurrentGameScore = 0,
        winner = null;

  GameState copyWith({
    bool? showCountDown,
    int? firstTotalScore,
    int? secondTotalScore,
    int? firstCurrentGameScore,
    int? secondCurrentGameScore,
    PlayerSide? winner,
  }) =>
      GameState(
        showCountDown: showCountDown ?? this.showCountDown,
        firstTotalScore: firstTotalScore ?? this.firstTotalScore,
        secondTotalScore: secondTotalScore ?? this.secondTotalScore,
        firstCurrentGameScore:
            firstCurrentGameScore ?? this.firstCurrentGameScore,
        secondCurrentGameScore:
            secondCurrentGameScore ?? this.secondCurrentGameScore,
        winner: winner ?? this.winner,
      );

  @override
  List<Object?> get props => [
        showCountDown,
        firstTotalScore,
        secondTotalScore,
        firstCurrentGameScore,
        secondCurrentGameScore,
        winner
      ];
}
