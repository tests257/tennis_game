part of '../game_part.dart';

class GameScreen extends StatelessWidget {
  const GameScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final firstPlayer = context.homeCubit.state.firstPlayer;
    final secondPlayer = context.homeCubit.state.secondPlayer;
    return Row(
      children: [
        Playground(
          player: firstPlayer,
          side: PlayerSide.left,
        ),
        const AppVerticalDivider(),
        Playground(
          player: secondPlayer,
          side: PlayerSide.right,
        ),
      ],
    );
  }
}
