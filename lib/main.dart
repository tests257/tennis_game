import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tennis_game/gen/assets.gen.dart';
import 'package:tennis_game/parts/home/home_part.dart';

void main() {
  runApp(const App());
}

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void didChangeDependencies() {
    precacheImage(Assets.images.court.provider(), context);
    precachePicture(Assets.icons.tennisball.svg().pictureProvider, context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tennis Game',
      theme: ThemeData.dark(useMaterial3: true),
      home: const HomePart(),
    );
  }
}
